package com.example.risk.dao;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "land")
public class Land {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Player player;

    @Column(name = "nb_troops")
    private int nbTroops;

    @ManyToMany
    @JoinTable(
            name = "neighbour",
            joinColumns = @JoinColumn(name = "id_neighbour1", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_neighbour2", referencedColumnName = "id")
    )
    @JsonIgnore
    private Set<Land> neighbours;


    @ManyToOne
    @JoinColumn(name = "game_id")
    @JsonBackReference
    private Game game;

    public Land() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public int getNbTroops() {
        return nbTroops;
    }

    public void setNbTroops(int nbTroops) {
        this.nbTroops = nbTroops;
    }

    public Set<Land> getNeighbours() {
        return neighbours;
    }

    public void setNeighbours(Set<Land> neighbours) {
        this.neighbours = neighbours;
    }

    public Game getGame() {
        return game;
    }

    public void setGame2(Game game) {
        this.game = game;
    }
}
