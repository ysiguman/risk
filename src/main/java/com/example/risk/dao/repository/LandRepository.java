package com.example.risk.dao.repository;

import com.example.risk.dao.Land;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LandRepository extends CrudRepository<Land, Long> {
    List<Land> findAll();
}
