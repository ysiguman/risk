package com.example.risk.dao.repository;

import com.example.risk.dao.Player;
import org.springframework.data.repository.CrudRepository;

public interface PlayerRepository extends CrudRepository<Player, Long> {
}
