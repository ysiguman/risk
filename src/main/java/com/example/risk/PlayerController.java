package com.example.risk;

import com.example.risk.dao.Player;
import com.example.risk.dao.repository.GameRepository;
import com.example.risk.dao.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "/player")
public class PlayerController {
    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private GameRepository gameRepository;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Player getPlayer(@PathVariable(name = "id") Long id) {
        Player player =  playerRepository.findOne(id);
        return player;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public Player create(
            @RequestParam(value = "name") String name,
            @RequestParam(value = "gameId") Long gameId) {
        Player player = new Player();
        player.setName(name);
        player.setGame(gameRepository.findOne(gameId));
        return playerRepository.save(player);
    }

    @RequestMapping(value = "/create-players", method = RequestMethod.POST)
    public Set<Player> create(
            @ModelAttribute(value = "players") List<String> names,
            @RequestParam(value = "gameId") Long gameId) {
        Set<Player> players = new HashSet<>();
        for (String name : names) {
            Player player = new Player();
            player.setName(name);
            player.setGame(gameRepository.findOne(gameId));
            players.add(playerRepository.save(player));
        }
        return players;
    }
}
