package com.example.risk;

import com.example.risk.dao.Land;
import com.example.risk.dao.Player;
import com.example.risk.dao.repository.LandRepository;
import com.example.risk.dao.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

/**
 * Created by ysiguman on 25/11/17.
 */
@RestController
@RequestMapping(value = "/land")
public class FieldController {
    @Autowired
    private LandRepository landRepository;
    @Autowired
    private LandService landService;
    @Autowired
    private PlayerRepository playerRepository;

    @RequestMapping(value = "/get-lands", method = RequestMethod.GET)
    public List<Land> getLands() {
        return landRepository.findAll();
    }

    @RequestMapping(value = "/land/{idFrom}/move-troops-to", method = RequestMethod.PUT)
    public String moveFromTo(
            @PathVariable(name = "idFrom")  Long idFrom,
            @RequestParam(name = "idTo")    Long idTo,
            @RequestParam(name = "idPlayer") Long idPlayer,
            @RequestParam(name = "nbTroops") int  nbTroops) {
        Land landFrom   = landRepository.findOne(idFrom);
        Land landTo     = landRepository.findOne(idTo);

        if (landService.isSamePlayer(landFrom, landTo, idPlayer)) {
            return "Les players ne correpondent pas";
        } else {
            if (!landService.isNeighbour(landFrom, landTo)) {
                return "Les territoires ne sont pas voisins";
            } else {
                if (landFrom.getNbTroops() - nbTroops < 1) {
                    return "Pas assez de troupes";
                } else {
                    landFrom.setNbTroops(landFrom.getNbTroops() - nbTroops);
                    landTo.setNbTroops(landTo.getNbTroops() + nbTroops);

                    landRepository.save(landFrom);
                    landRepository.save(landTo);
                    return "ok";
                }
            }
        }
    }
    @RequestMapping(value = "/land/{idFrom}/fight-to", method = RequestMethod.PUT)
    public String fightFromTo(
            @PathVariable(name = "idFrom")  Long idFrom,
            @RequestParam(name = "idTo")    Long idTo,
            @RequestParam(name = "idPlayer") Long idPlayer,
            @RequestParam(name = "nbTroops") int  nbTroops) {
        Land landFrom = landRepository.findOne(idFrom);
        Land landTo = landRepository.findOne(idTo);

        if (Objects.equals(landFrom.getPlayer().getId(), landTo.getPlayer().getId()) ||
                !Objects.equals(landFrom.getPlayer().getId(), idPlayer)) {
            return "Les players ne correpondent pas";
        } else {
            if (!landService.isNeighbour(landFrom, landTo)) {
                return "Les territoires ne sont pas voisins";
            } else {
                if (landFrom.getNbTroops() - nbTroops < 1) {
                    return "Pas assez de troupes";
                } else {
                    landFrom.setNbTroops(landFrom.getNbTroops() - nbTroops);
                    landTo.setNbTroops(landTo.getNbTroops() - nbTroops);

                    landRepository.save(landFrom);
                    landRepository.save(landTo);
                    return "ok";
                }
            }
        }
    }

    @RequestMapping(value = "/{id}/set-player", method = RequestMethod.POST)
    public String setPlayer(
            @PathVariable(name = "id") Long idLand,
            @RequestParam(name = "idPlayer") Long idPlayer) {
        Land land = landRepository.findOne(idLand);
        Player player = playerRepository.findOne(idPlayer);
        if (land.getPlayer() != null) {
            return "Territoire déjà attribué";
        } else {
            land.setPlayer(player);
            landRepository.save(land);
            return "ok";
        }
    }
}
