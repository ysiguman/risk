package com.example.risk;

import com.example.risk.dao.Game;
import com.example.risk.dao.Player;
import com.example.risk.dao.repository.GameRepository;
import com.example.risk.dao.repository.LandRepository;
import com.example.risk.dao.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class GameController {
    @Autowired
    GameRepository gameRepository;

    @Autowired
    LandRepository landRepository;

    @Autowired
    PlayerRepository playerRepository;

    @RequestMapping(value = "/getGames", method = RequestMethod.GET)
    public List<Game> getGames() {
        return gameRepository.findAll();
    }


    @RequestMapping(value = "/getGame", method = RequestMethod.GET)
    public Game getGames(@RequestParam("id") Long id) {
        return gameRepository.findOne(id);
    }

    @RequestMapping(value = "/create-game", method = RequestMethod.POST)
    public Game createGame() {
        return gameRepository.save(new Game());
    }
}
