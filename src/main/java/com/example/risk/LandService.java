package com.example.risk;

import com.example.risk.dao.Land;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * Created by ysiguman on 25/11/17.
 */
@Component
public class LandService {
    public boolean isNeighbour(Land land1, Land land2) {
        for (Land landNear : land1.getNeighbours()) {
            if (landNear.equals(land2))
                return true;
        }
        return false;
    }

    public boolean isSamePlayer(Land land1, Land land2, Long idPlayer) {
        return !Objects.equals(land1.getPlayer().getId(), land2.getPlayer().getId()) ||
                !Objects.equals(land1.getPlayer().getId(), idPlayer);
    }
}
